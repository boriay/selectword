import sublime
import sublime_plugin


class EventProcessor(sublime_plugin.EventListener):
    def run(self, edit):
        pass

    def on_selection_modified(self, view):
        self.clear_highlighted_status(view)

    def clear_highlighted_status(self, view):
        s = view.get_status("0_highlight")
        if s and not s.startswith("highlight"):
            view.set_status("0_highlight", "highlighted " + s)
        else:
            view.erase_status("0_highlight")
            view.get_regions("mark")
            view.erase_regions("mark")


class MarkWordsCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        r = self.view.sel()[0]
        if r.empty():
            r = self.view.word(r)
        w = self.view.substr(r)
        if w:
            old_scope = self.view.settings().get('syntax')
            #self.view.set_syntax_file('Plain text.tmLanguage')
            rr = self.view.find_all(w, sublime.LITERAL)
            self.view.set_status("0_highlight", "{0}".format(len(rr)))
            self.view.add_regions("mark", rr, "mark", "dot")
            self.view.set_syntax_file(old_scope)
